package fr.univtours.polytech.tpnote;

public enum Categorie {
    BLANC,
    NOIR,
    VERT,
    OOLONG,
    AROMATISE
}