package fr.univtours.polytech.tpnote;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
 
    	// Création d'un stock de thé
        StockDeThe stock = new StockDeThe();

        // Création de quelques thés
        The the1 = new The("Gyokuro", Categorie.VERT, "Japon", 16, 9.5);
        The the2 = new The("Earl Grey", Categorie.NOIR, "Angleterre", 17, 8.0);
        The the3 = new The("Matcha", Categorie.VERT, "Japon", 18, 9.0);

        // Ajout des thés au stock
        stock.ajouter(the1, 200);
        stock.ajouter(the2, 100);
        stock.ajouter(the3, 150);

        // Affichage du stock
        System.out.println("Stock de thés :\n" + stock);

        // Vérification de la présence d'un thé dans le stock
        System.out.println("Le stock contient le thé 'Gyokuro' : " + stock.contientThe(the1));
        System.out.println("Le stock contient le thé 'Earl Grey' : " + stock.contientThe(the2));

        // Récupération des thés de catégorie 'VERT'
        Set<The> thesVerts = stock.theDeCategorie(Categorie.VERT);
        System.out.println("\nThés verts dans le stock :");
        for (The the : thesVerts) {
            System.out.println(the);
        }

        // Récupération des thés avec une note supérieure ou égale à 8.5
        Set<The> thesNoteSupOuEgale = stock.thesDontLaNoteEstSupOuEgaleA(8.5);
        System.out.println("\nThés avec une note supérieure ou égale à 8.5 :");
        for (The the : thesNoteSupOuEgale) {
            System.out.println(the);
        }

        // Tri des thés par quantités croissantes
        List<The> thesTries = stock.trierParQuantiteCroissante();
        System.out.println("\nThés triés par quantités croissantes :");
        for (The the : thesTries) {
            System.out.println(the + " - Quantité : " + stock.getQuantite(the) + "g");
        }
    }
    	
    	
    	/*---------- Question 3 -----------*/  
    	
    	/*StockDeThe stock = new StockDeThe();

        The gyokuro = new The("Gyokuro", Categorie.VERT, "Japon", 78, 9.5);
        The darjeeling = new The("Darjeeling", Categorie.VERT, "Inde", 85, 8.0);
        The oolong = new The("Oolong", Categorie.VERT, "Taiwan", 90, 7.5);
        The sencha = new The("Sencha", Categorie.VERT, "Japon", 75, 8.7);
        The assam = new The("Assam", Categorie.VERT, "Inde", 95, 7.2);

        stock.ajouter(gyokuro, 200);
        stock.ajouter(darjeeling, 150);
        stock.ajouter(oolong, 100);
        stock.ajouter(sencha, 120);
        stock.ajouter(assam, 80);

        Set<The> thesVerts = stock.theDeCategorie(Categorie.VERT);
        System.out.println("Thés verts dans le stock :");
        for (The the : thesVerts) {
            System.out.println(the.getNom());
        }

        Set<The> thesNoteSupOuEgale = stock.thesDontLaNoteEstSupOuEgaleA(8.5);
        System.out.println("\nThés avec une note supérieure ou égale à 8.5 :");
        for (The the : thesNoteSupOuEgale) {
            System.out.println(the.getNom());
        }
    }*/
    	
    	
    	
    	/*---------- Question 2 -----------*/    	
    	
    	/*StockDeThe stock = new StockDeThe();

        The gyokuro = new The("Gyokuro", Categorie.VERT, "Japon", 78, 9.5);
        The darjeeling = new The("Darjeeling", Categorie.NOIR, "Inde", 85, 8.0);

        stock.ajouter(gyokuro, 200);
        stock.ajouter(darjeeling, 150);

        System.out.println(stock.toString());

        System.out.println("Contient Gyokuro ? " + stock.contientThe(gyokuro));
        System.out.println("Contient Oolong ? " + stock.contientThe(new The("Oolong", Categorie.OOLONG, "Taiwan", 90, 7.5)));

        stock.sortir(gyokuro, 100);

        System.out.println(stock.toString());

        try {
            stock.sortir(gyokuro, 150); // Tentative de retirer une quantité supérieure à celle disponible
        } catch (IllegalArgumentException e) {
            System.out.println("Erreur : " + e.getMessage());
        }*/    	
    	
    	
    	
    	
    	/*---------- Question 2 -----------*/
    	/*// Création d'un thé
        The gyokuro = new The("Gyokuro", Categorie.VERT, "Japon", 78, 9.5);

        // Affichage des informations du thé
        System.out.println(gyokuro.toString());

        // Modification de la note d'appréciation du thé
        gyokuro.setNoteAppreciation(8.7);

        // Affichage des nouvelles informations du thé
        System.out.println("\nAprès modification de la note d'appréciation :\n" + gyokuro.toString());
        
        */
    
}

