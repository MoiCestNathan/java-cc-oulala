package fr.univtours.polytech.tpnote;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StockDeThe {
    private Map<The, Integer> stock;

    public StockDeThe() {
        stock = new HashMap<>();
    }

    public boolean contientThe(The the) {
        return stock.containsKey(the);
    }

    public void ajouter(The the, int nbGrammes) {
    	if (nbGrammes < 0) {
    	    throw new IllegalArgumentException("La quantité du thé à ajouter doit être supérieure ou égale à 0.");
    	}

    	int quantiteActuelle = stock.getOrDefault(the, 0);
    	stock.put(the, quantiteActuelle + nbGrammes);
    }

    public void sortir(The the, int nbGrammes) {
        if (nbGrammes < 0) {
            throw new IllegalArgumentException("La quantité ne peut pas être négative.");
        }

        int quantiteActuelle = stock.getOrDefault(the, 0);
        
        if (quantiteActuelle < nbGrammes) {
            throw new IllegalArgumentException("La quantité demandée est supérieure à celle disponible en stock.");
       
        } else if (quantiteActuelle == nbGrammes) {
            stock.remove(the); // Supprime le thé du stock si la quantité demandée est égale à la quantité en stock
        
        } else {
            stock.put(the, quantiteActuelle - nbGrammes);
        }
    }

    public Set<The> theDeCategorie(Categorie cat) {
        Set<The> thesDeCategorie = new HashSet<>();

        for (The the : stock.keySet()) {
            if (the.getCategorie() == cat) {
                thesDeCategorie.add(the);
            }
        }

        return thesDeCategorie;
    }
    
    public Set<The> thesDontLaNoteEstSupOuEgaleA(double seuilNote) {
        Set<The> thesNoteSupOuEgale = new HashSet<>();

        for (The the : stock.keySet()) {
            if (the.getNoteAppreciation() >= seuilNote) {
                thesNoteSupOuEgale.add(the);
            }
        }

        return thesNoteSupOuEgale;
    }
    
    public List<The> trierParQuantiteCroissante() {
        List<The> thes = new ArrayList<>(stock.keySet());
        Collections.sort(thes, Comparator.comparingInt(the -> stock.get(the)));
        return thes;
    }

    public int getQuantite(The the) {
        return stock.getOrDefault(the, 0);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Stock de thés :\n");

        for (Map.Entry<The, Integer> entry : stock.entrySet()) {
            The the = entry.getKey();
            int quantite = entry.getValue();

            sb.append(the.getNom()).append(" - ").append(the.getCategorie()).append(" - ").append(quantite).append("g\n");
        }

        return sb.toString();
    }
}
