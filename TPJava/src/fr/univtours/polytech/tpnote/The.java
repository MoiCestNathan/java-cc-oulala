package fr.univtours.polytech.tpnote;

public class The {
    private String nom;
    private Categorie categorie;
    private String paysOrigine;
    private int temperatureInfusion;
    private double noteAppreciation;

    public The(String nom, Categorie categorie, String paysOrigine, int temperatureInfusion, double noteAppreciation) {
    	if (nom == null || nom.isEmpty()) {
            throw new IllegalArgumentException("Le nom du thé ne peut pas être null ou vide.");
        }
        if (temperatureInfusion < 15) {
            throw new IllegalArgumentException("La température d'infusion doit être supérieure ou égale à 15°C.");
        }
        
    	this.nom = nom;
        this.categorie = categorie;
        this.paysOrigine = paysOrigine;
        this.temperatureInfusion = temperatureInfusion;
        this.noteAppreciation = noteAppreciation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getPaysOrigine() {
        return paysOrigine;
    }

    public void setPaysOrigine(String paysOrigine) {
        this.paysOrigine = paysOrigine;
    }

    public int getTemperatureInfusion() {
        return temperatureInfusion;
    }

    public void setTemperatureInfusion(int temperatureInfusion) {
        this.temperatureInfusion = temperatureInfusion;
    }

    public double getNoteAppreciation() {
        return noteAppreciation;
    }

    public void setNoteAppreciation(double noteAppreciation) {
        this.noteAppreciation = noteAppreciation;
    }

    @Override
    public String toString() {
        return "Nom: " + nom +
                "\nCatégorie: " + categorie +
                "\nPays d'origine: " + paysOrigine +
                "\nTempérature d'infusion: " + temperatureInfusion +
                "\nNote d'appréciation: " + noteAppreciation;
    }
}    
