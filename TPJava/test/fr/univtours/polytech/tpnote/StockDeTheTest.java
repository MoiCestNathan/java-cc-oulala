package fr.univtours.polytech.tpnote;
import org.junit.Assert;
import org.junit.Test;

public class StockDeTheTest {

    @Test
    public void testTheNomNullThrowsException() {
        try {
            new The(null, Categorie.NOIR, "Pays", 80, 8.0);
            Assert.fail("La création d'un thé avec un nom null devrait lever une exception.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Le nom du thé ne peut pas être null ou vide.", e.getMessage());
        }
    }

    @Test
    public void testTheTemperatureInf15ThrowsException() {
        try {
            new The("Thé", Categorie.NOIR, "Pays", 10, 8.0);
            Assert.fail("La création d'un thé avec une température d'infusion inférieure à 15°C devrait lever une exception.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("La température d'infusion doit être supérieure ou égale à 15°C.", e.getMessage());
        }
    }

    @Test
    public void testAjoutTheQuantiteNegativeThrowsException() {
        StockDeThe stock = new StockDeThe();
        The the = new The("Thé", Categorie.NOIR, "Pays", 80, 8.0);

        try {
            stock.ajouter(the, -50);
            Assert.fail("L'ajout d'un thé avec une quantité négative devrait lever une exception.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("La quantité du thé à ajouter doit être supérieure ou égale à 0.", e.getMessage());
        }
    }

    @Test
    public void testSortieQuantiteTropGrandeThrowsException() {
        StockDeThe stock = new StockDeThe();
        The the = new The("Thé", Categorie.NOIR, "Pays", 80, 8.0);
        stock.ajouter(the, 100);

        try {
            stock.sortir(the, 150);
            Assert.fail("La sortie d'une quantité trop grande d'un thé du stock devrait lever une exception.");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("La quantité demandée est supérieure à celle disponible en stock.", e.getMessage());
        }
    }
}
